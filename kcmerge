#!/bin/bash

NEWURL=""
OLDURL="127.0.0.1"
NEWCONFIG=""
OLDCONFIG="~/.kube/config"
COMMAND="empty"

help() {
    echo "------------------------------------------------"
    # echo "Example usage: $BASH_SOURCE -n /path/to/new/config"
    echo "Options:"
    echo " -o /path/to/old/kubeconfig, default: $OLDCONFIG"
    echo " -n /path/to/new/kubeconfig"
    echo " -u old.k8s.url  (cluster url to be replaced, default: $OLDURL)"
    echo " -r new.k8s.url  (new cluster url to replace old url"
    echo " -m yes          (Confirm merge of $NEWCONFIG with $OLDCONFIG)"
    echo " -h | h | --help (Print this Help)"
    echo 
    echo "Usage:"
    echo " $BASH_SOURCE -n /path/to/new/config -u old.k8s.url -r new.k8s.url -c r"
    echo " $BASH_SOURCE -n /path/to/new/config -r new.k8s.url -c r"
    echo " $BASH_SOURCE -n /path/to/new/config -c m"
    echo " $BASH_SOURCE -o /path/to/old/kubeconfig -n /path/to/new/config -c merge"
    echo " $BASH_SOURCE -r new.k8s.url -n /path/to/new/config -c mr"
    echo " $BASH_SOURCE -u old.k8s.url -r new.k8s.url -o /path/to/old/kubeconfig -n /path/to/new/config -c mr"
    echo "------------------------------------------------"
}

replace() {
    if [[ "$NEWURL" ==  ""  || "$NEWCONFIG" == "" ]]; then
        echo "New URL or NEWCONFIG not set, nothing to replace."
    else
        echo -e "Replacing $OLDURL \n with $NEWURL \n in $NEWCONFIG"
        eval "sed -i 's+$OLDURL+$NEWURL+g' $NEWCONFIG"
    fi
}

merge() {
    if [ "$NEWCONFIG" ==  "" ]; then
        echo "New config not set, nothing to merge." 
    else
        echo "Merging files:"
        echo "   $OLDCONFIG"
        echo "   $NEWCONFIG"

        echo "Backing up current kubeconfig"
        eval "cp $OLDCONFIG $OLDCONFIG.backup.$(date +%F-%H:%M:%S)"
        echo "Merging kubeconfigs"
        eval "KUBECONFIG=$OLDCONFIG:$NEWCONFIG kubectl config view --flatten > /tmp/config" 
        eval "mv /tmp/config $OLDCONFIG"
        echo "Done"
    fi
}

run_command() {
    if [ "$COMMAND" ==  "m" ]; then merge
    elif [ "$COMMAND" ==  "r" ]; then replace
    elif [ "$COMMAND" ==  "mr" ]; then 
        replace
        merge
    else
        echo -e "Unknown command, nothing to do."
    fi
}

if [ "$#" -eq  "0" ];  then
    echo "No arguments supplied"
    help && exit 1
fi

if [[ "$1" = "h" || "$1" = "-h" || "$1" = "--help" ]];  then
    help && exit 1
fi

while getopts "u:r:o:n:c:" OPTION; do
    case ${OPTION} in
    u)  OLDURL=$OPTARG    ;;
    r)  NEWURL=$OPTARG    ;;
    o)  OLDCONFIG=$OPTARG ;;
    n)  NEWCONFIG=$OPTARG ;;  
    c)  COMMAND=$OPTARG
        run_command   ;;
    *)  echo "Unknown option or missing argument, quitting." && exit 1  ;;
    esac
done

