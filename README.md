# kubernetes-power-tools
How to get Helm, kubectl, kubectx, kubens, kube-ps1, k9s and autocomplete up and running to increase efficiency while working with kubernetes clusters.

![kubernetes](https://raw.githubusercontent.com/kubernetes/kubernetes/master/logo/name_blue.png)

Credit:

1. https://kubernetes.io/docs/tasks/tools/
2. https://helm.sh/docs/intro/install/
3. https://github.com/ahmetb/kubectx
4. https://github.com/cykerway/complete-alias
5. https://github.com/jonmosco/kube-ps1
6. https://github.com/derailed/k9s

## kubectl, helm, ...

### The universal way - using cURL
```bash
## kubectl
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"

sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

## kubectx and kubens
sudo git clone https://github.com/ahmetb/kubectx /opt/kubectx
sudo ln -s /opt/kubectx/kubectx /usr/local/bin/kubectx
sudo ln -s /opt/kubectx/kubens /usr/local/bin/kubens

## helm
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```

### The Debian/Ubuntu way - using apt
```bash
## install necessary tools
sudo apt install -y ca-certificates apt-transport-https curl 

## add kubernetes repository
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list

## add helm repository
curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list

## install kubernetes tools
sudo apt update
sudo apt install -y helm kubectl kubectx
```

## Autocompletion, Aliases, prompt, ...

### Stress-free setup using my preconfigured bash_kube_aliases file
1) Prerequisites
```bash
sudo apt install -y curl fzf bash-completion

mkdir ~/.bash_completion.d
curl https://raw.githubusercontent.com/cykerway/complete-alias/master/complete_alias >> ~/.bash_completion.d/complete_alias 
mkdir ~/.kube
curl https://raw.githubusercontent.com/jonmosco/kube-ps1/master/kube-ps1.sh >> ~/.kube/kube-ps1.sh

# Tweak kube-ps1.sh to work with kcx
sed -i 's/stat -L -c/stat -c/g' ~/.kube/kube-ps1.sh
sed -i 's/KUBE_PS1_LAST_TIME=$(/KUBE_PS1_LAST_TIME=0 #$(/g' kube_ps1.sh
```
2) Download and enable .bash_kube_aliases
```bash
wget -O ~/.bash_kube_aliases https://gitlab.com/janjansmid/kubernetes-power-tools/-/raw/main/bash_kube_aliases

cat >> ~/.bashrc << EOF
##----------------------------------
if [ -f ~/.bash_kube_aliases ]; then
    . ~/.bash_kube_aliases
fi
##----------------------------------
EOF
```
3) Close and open your terminal
```bash
## or just
source ~/.bashrc
```
### Manual setup
#### Backup .bashrc - so you dont hate me
```bash
cp ~/.bashrc ~/.bashrc.backup
```

#### Autocompletion
```bash
sudo apt install -y bash-completion 

cat >> ~/.bashrc<< EOF
##-----kubernetes tools bash completion-----"
source <(kubectl completion bash)"
source <(helm completion bash)"
EOF
```   
#### Aliases
```bash
## 'install' complete_alias
mkdir ~/.bash_completion.d
curl https://raw.githubusercontent.com/cykerway/complete-alias/master/complete_alias >> ~/.bash_completion.d/complete_alias  

cat >> ~/.bashrc<< EOF
source ~/.bash_completion.d/complete_alias

##-----kubernetes tools aliases-----
alias k='kubectl'" 
complete -F _complete_alias k"
alias kn='kubens'" 
complete -F _complete_alias kn"
alias kx='kubectx'"
complete -F _complete_alias kx"
EOF

cat >> ~/.bashrc<< EOF
##-----kubernetes context switcher-----
kcx() {
  (
    cd ~/.kube/ || exit 1
    # shellcheck disable=SC2012
    KUBE=$(ls *.yaml *config | fzf)
    [[ -n "$KUBE" ]] && ln -sf "$KUBE" config && chmod 600 config
  )
}
EOF
```

##### Usage:
  - *kcx default*
    - switch between contexts in multiple kubeconfigs
    - ~/.kube/config is symlink to selected kubeconfig
  - *kx minikube*
    - switch between contexts in single kubeconfig 
  - *kn kube-system*
    - switch namespace
  - *k get nodes*
    - kubectl ... 
    
    
#### kube-ps1 colored prompt
![cloud-init](kube-ps1.png)
```bash
curl https://raw.githubusercontent.com/jonmosco/kube-ps1/master/kube-ps1.sh >> ~/.kube/kube-ps1.sh

cat >> ~/.bashrc<< EOF
##-----kubernetes colored prompt-----
function kubeon() {
  source ~/.kube/kube-ps1.sh
  #export KUBE_PS1_SYMBOL_ENABLE=false
  export PS1=$PS1'$(kube_ps1): '
}
EOF

# Tweak kube-ps1.sh to work with kcx
sed -i 's/stat -L -c/stat -c/g' ~/.kube/kube-ps1.sh
sed -i 's/KUBE_PS1_LAST_TIME=$(/KUBE_PS1_LAST_TIME=0 #$(/g' ~/.kube/kube-ps1.sh
```
##### Usage: 
  - kubeon to activate 
  - kubeoff do deactivate


-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------
## Another kubernetes tools, tips & tricks
### k9s management console for terminal
    ____  __.________        
    |    |/ _/   __   \______ 
    |      < \____    /  ___/ 
    |    |  \   /    /\___ \  
    |____|__ \ /____//____  > 
            \/            \/

https://k9scli.io/
```bash
## install k9s
wget -O k9s_linux_amd64.deb https://github.com/derailed/k9s/releases/download/v0.31.5/k9s_linux_amd64.deb
sudo dpkg -i k9s_linux_amd64.deb
```

### Remove cluster, context and users from kubeconfig
```bash
kubectl config delete-cluster default
kubectl config delete-context default
kubectl config delete-user default
```    
    
### Combine multiple kubeconfigs
```bash
KUBECONFIG=~/.kube/config:~/.kube/new-cluster-config kubectl config view --flatten > /tmp/config
mv ~/.kube/config ~/.kube/config.old
mv /tmp/config ~/.kube/config
```


### Change new cluster URL in new kubeconfig
```bash
sed -in 's/127.0.0.1/new.cluster.url/g' ~/.kube/config
chmod 600 ~/.kube/config
```

### Openlens
- https://github.com/MuhammedKalkan/OpenLens/releases
- https://github.com/alebcay/openlens-node-pod-menu
  - File - Extensions - @alebcay/openlens-node-pod-menu